import json
import tornado.web
from bson import ObjectId
from bson.errors import InvalidId
from tornado.web import RequestHandler

from utils import find_best_coupon

class BaseHandler(RequestHandler):

    @property
    def db(self):
        return self.application.db


class DiscountHandler(BaseHandler):
    """Print 'Hello, world!' as the response body."""

    async def get(self):
        """Handle a GET request for saying Hello World!."""
        user = self.get_arguments('user')
        service =  self.get_arguments('service')
        
        # schema validation
        if not user or not service:
            self.set_status(422)
            return self.finish({ "erro": True, "msg": "Invalid Entity" })    

        self.write("Hello, world!")
    
    async def post(self):
        
        data = json.loads(self.request.body)
        user_id = data.get('user_id', None)
        service_value = data.get('service_value', None)
        coupons_list = []

        try:
            ObjectId(user_id)
        except InvalidId as error:
            return self.finish({ "error": True, "msg": str(error) })    

        if not user_id or not service_value:
            self.set_status(422)
            return self.finish({ "error": True, "msg": "Invalid Entity" })    

        # grab all the coupons name
        coupons = await self.db['1help']['users'].find_one({"_id": ObjectId(user_id)}, projection={"coupons": True})
        # sem cupons
        if not coupons.get('coupons'):
            self.set_status(200)
            return self.finish({"discount": 0, "value_with_discount": service_value } )
        
        for coupon in coupons.get('coupons'):
            data = await self.db['1help']['coupons'].find_one({"name": coupon})
            coupons_list.append(data)
        
        # find the most optimun coupon
        best_coupon = find_best_coupon(coupons_list, service_value)
        print(best_coupon)
        # remove it from the user
        await self.db['1help']['users'].update_one( {"_id": ObjectId(user_id)} ,{"$pull": {"coupons": { "$in": [ best_coupon[0]['name']]   }}} )
        # return 
        self.set_status(200)
        self.write({"discount": best_coupon[0]['value'], "coupon": best_coupon[0]['name'], "value_with_discount": best_coupon[1] } )
