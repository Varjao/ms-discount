import os
import tornado
import motor

from views import DiscountHandler

from dotenv import load_dotenv
load_dotenv()


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            ("/discount", DiscountHandler),
        ]
        
        super(Application, self).__init__(handlers)
        self.db = motor.motor_tornado.MotorClient(os.environ.get('MONGODB_URI'))
        

def main():
    tornado.log.enable_pretty_logging()

    PORT = os.environ.get('PORT')

    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(8080)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()