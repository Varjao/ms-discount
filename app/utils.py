from typing import List

def find_best_coupon(coupon_list: List, service_value):
    _coupon = []
    
    for coupons in coupon_list:
        
        if coupons['type'] == 'PERCENTUAL':
            value = service_value - (coupons['value']/100 * service_value) 
        if coupons['type'] == 'ABSOLUTE':
            value = service_value - coupons['value']
        if value > 0:
            _coupon.append((coupons, value))
    
    return sorted( _coupon , key=lambda x: x[1] )[0]
